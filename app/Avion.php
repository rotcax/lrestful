<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Avion extends Model
{
    protected $table = "aviones";
    protected $primaryKey = "serie";
    protected $fillable = [];
    public $timestamps = false;

    public function fabricante()
	{
		return $this->belogsTo('App\Fabricante');
	}
}

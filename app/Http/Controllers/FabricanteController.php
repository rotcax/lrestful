<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Fabricante;
use Response;

class FabricanteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.basic', ['only'=>['store', 'update', 'destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $fabricante = Cache::remember('cachefabricante', 15/60, function(){
            return Fabricante::simplePaginate(10);
        });

        return response()->json(['status'=>'ok', 'siguiente'=>$fabricante->nextPageUrl(), 'anterior'=>$fabricante->previousPageUrl(), 'data'=>$fabricante->items()], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return "Se muestra formulario para crear un fabricante.";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->input('nombre') || !$request->input('direccion') || !$request->input('telefono')){

            return response()->json(['errors'=>array(['code'=>422, 'message'=>'Faltan datos necesarios para el proceso de alta.'])], 422);
        }

        $nuevoFabricante = Fabricante::create($request->all());

        $response = Response::make(json_encode(['data'=>$nuevoFabricante]), 201)->header('Location', 'http://localhost:8000/fabricantes/'.$nuevoFabricante->id)->header('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $fabricante = Fabricante::find($id);

        if(!$fabricante){

            return response()->json(['errors'=>array('code'=>404,'message'=>'No se encuentra un fabricante con ese codigo.')], 404);
        }

        return response()->json(['status'=>'ok', 'data'=>$fabricante], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $fabricante = Fabricante::find($id);

        if(!$fabricante){

            return response()->json(['errors'=>(['code'=>404, 'message'=>'No se encuentra un fabricante con ese codigo.'])], 404);
        }

        $nombre = $request->input('nombre');
        $direccion = $request->input('direccion');
        $telefono = $request->input('telefono');

        if($request->method() === 'PATCH'){

            $bandera = false;

            if($nombre){

                $fabricante->nombre = $nombre;
                $bandera = true;
            }

            if($direccion){

                $fabricante->direccion = $direccion;
                $bandera = true;
            }

            if($telefono){

                $fabricante->telefono = $telefono;
                $bandera = true;
            }

            if($bandera){

                $fabricante->save();

                return response()->json(['status'=>'ok', 'data'=>$fabricante], 200);

            }else{

                return response()->json(['errors'=>array('code'=>304, 'message'=>'No se ha modificado ningun dato de fabricante.')], 304);
            }
        }

        if(!$nombre || !$direccion || !$telefono){

            return response()->json(['errors'=>array('code'=>422, 'message'=>'Faltan valores para completar el procesamiento')], 422);
        }

        $fabricante->nombre = $nombre;
        $fabricante->direccion = $direccion;
        $fabricante->telefono = $telefono;

        $fabricante->save();

        return response()->json(['status'=>'ok', 'data'=>$fabricante], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fabricante = Fabricante::find($id);

        if(!$fabricante){

            return response()->json(['errors'=>array(['code'=>404, 'message'=>'No se encuentra un fabricante con ese codigo.'])], 404);
        }

        $aviones = $fabricante->aviones;

        if(sizeof($aviones) > 0){

            return response()->json(['code'=>409, 'message'=>'Este fabricante posee aviones y no puede ser eliminado.'], 409);
        }

        $fabricante->delete();

        return response()->json(['code'=>204, 'message'=>'Se ha eliminado el fabricante correctamente'], 204);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Fabricante;
use App\Avion;
use Response;

class FabricanteAvionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.basic', ['only'=>['store', 'update', 'destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($idFabricante)
    {
        $fabricante = Fabricante::find($idFabricante);

        if(!$fabricante){

            return response()->json(['errors'=>array(['code'=>404, 'message'=>'No se encuentra un fabricante con ese codigo.'])], 404);
        }

        $avionesFabri = Cache::remember('claveAviones', 2, function() use($fabricante){
            return $fabricante->aviones()->get();
        });

        return response()->json(['status'=>'ok', 'data'=>$fabricante->$avionesFabri], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($idFabricante)
    {
        return "Se muestra el formulario para crear un avion del fabricante $idFabricante.";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->input('modelo') || !$request->input('longitud') || !$request->input('capacidad') || !$request->input('velocidad') || !$request->input('alcance')){

            return response()->json(['errors'=>array(['code'=>422, 'message'=>'Faltan datos necesarios para el proceso de alta.'])], 422);
        }

        $fabricante = Fabricante::find($idFabricante);

        if(!$fabricante){

            return response()->json(['errors'=>array(['code'=>404, 'message'=>'No se encuentra un fabricante con ese codigo'])], 404);
        }

        $nuevoAvion = $fabricante->aviones()->create($request->all());

        $response = Response::make(json_encode(['data'=>$nuevoAvion]), 201)->header('Location', 'http://localhost:8000/aviones/'.$nuevoAvion->serie)->header('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($idFabricante, $idAvion)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idFabricante, $idAvion)
    {
        return "Se muestra formulario para editar el avion $idAvion del fabricante $idFabricante";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idFabricante, $idAvion)
    {
        $fabricante = Fabricante::find($idFabricante);

        if(!$fabricante){

            return response()->json(['erros'=>array(['code'=>404, 'message'=>'No se encuentra un fabricante con ese codigo'])], 404);
        }

        $avion = $fabricante->aviones()->find($idAvion);

        if(!$avion){

            return response()->json(['errors'=>array(['code'=>404, 'message'=>'No se encuentra un avion con ese codigo asociado al fabricante.'])], 404);
        }

        $modelo = $request->input('modelo');
        $longitud = $request->input('longitud');
        $capacidad = $request->input('capacidad');
        $velocidad = $request->input('velocidad');
        $alcance = $request->input('alcance');

        if($request->method() === 'PATCH'){

            $bandera = false;

            if($modelo){

                $avion->modelo = $modelo;
                $bandera = true;
            }

            if($longitud){

                $avion->longitud = $longitud;
                $bandera = true;
            }

            if($capacidad){

                $avion->capacidad = $capacidad;
                $bandera = true;
            }

            if($velocidad){

                $avion->velocidad = $velocidad;
                $bandera = true;
            }

            if($alcance){

                $avion->alcance = $alcance;
                $bandera = true;
            }

            if($bandera){

                $avion->save();

                return response()->json(['status'=>'ok', 'data'=>$avion], 200);

            }else{

                return response()->json(['errors'=>array(['code'=>304, 'message'=>'No se ha modificado ningun dato del avion'])], 304);
            }
        }

        if(!$modelo || !$longitud || !$capacidad || !$velocidad || !$alcance){

            return response()->json(['erros'=>array(['code'=>422, 'message'=>'Faltan valores para completar el procesamiento.'])], 422);
        }

        $avion->modelo = $modelo;
        $avion->longitud = $longitud;
        $avion->capacidad = $capacidad;
        $avion->velocidad = $velocidad;
        $avion->alcance = $alcance;

        $avion->save();

        return response()->json(['status'=>'ok', 'data'=>$avion], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idFabricante, $idAvion)
    {
        $fabricante = Fabricante::find($idFabricante);

        if(!$fabricante){

            return response()->json(['errors'=>array(['code'=>404, 'message'=>'No se encuentra un fabricante con ese codigo'])], 404);
        }

        $avion = $fabricante->aviones()->find($idAvion);

        if(!$avion){

            return response()->json(['errors'=>array(['code'=>404, 'message'=>'No se encuentra un avion con ese codigo asociado a ese fabricante.'])], 404);
        }

        $avion->delete();

        return response()->json(['code'=>204, 'message'=>'Se ha eliminado el avion correctamente.'], 204);
    }
}

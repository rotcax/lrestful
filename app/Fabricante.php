<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fabricante extends Model
{
    protected $table = 'fabricantes';
    protected $primaryKey = 'id';
    protected $fillable = [];
    public $timestamps = false;

    public function aviones()
	{
		return $this->hasMany('App\Avion');
	}
}

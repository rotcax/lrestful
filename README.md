# Api RestFul realizada en Laravel 5.6

Rutas accesibles:

+--------+-----------+--------------------------------------------------+-----------------------------+--------------------------------------------------------+----------------+
| Domain | Method    | URI                                              | Name                        | Action                                                 | Middleware     |
+--------+-----------+--------------------------------------------------+-----------------------------+--------------------------------------------------------+----------------+
|        | GET|HEAD  | api/user                                         |                             | Closure                                                | api,auth:api   |
|        | GET|HEAD  | api/v1/aviones                                   | aviones.index               | App\Http\Controllers\AvionController@index             | web            |
|        | GET|HEAD  | api/v1/aviones/{avione}                          | aviones.show                | App\Http\Controllers\AvionController@show              | web            |
|        | GET|HEAD  | api/v1/fabricantes                               | fabricantes.index           | App\Http\Controllers\FabricanteController@index        | web            |
|        | POST      | api/v1/fabricantes                               | fabricantes.store           | App\Http\Controllers\FabricanteController@store        | web,auth.basic |
|        | GET|HEAD  | api/v1/fabricantes/{fabricante}                  | fabricantes.show            | App\Http\Controllers\FabricanteController@show         | web            |
|        | PUT|PATCH | api/v1/fabricantes/{fabricante}                  | fabricantes.update          | App\Http\Controllers\FabricanteController@update       | web,auth.basic |
|        | DELETE    | api/v1/fabricantes/{fabricante}                  | fabricantes.destroy         | App\Http\Controllers\FabricanteController@destroy      | web,auth.basic |
|        | GET|HEAD  | api/v1/fabricantes/{fabricante}/aviones          | fabricantes.aviones.index   | App\Http\Controllers\FabricanteAvionController@index   | web            |
|        | POST      | api/v1/fabricantes/{fabricante}/aviones          | fabricantes.aviones.store   | App\Http\Controllers\FabricanteAvionController@store   | web,auth.basic |
|        | PUT|PATCH | api/v1/fabricantes/{fabricante}/aviones/{avione} | fabricantes.aviones.update  | App\Http\Controllers\FabricanteAvionController@update  | web,auth.basic |
|        | DELETE    | api/v1/fabricantes/{fabricante}/aviones/{avione} | fabricantes.aviones.destroy | App\Http\Controllers\FabricanteAvionController@destroy | web,auth.basic |
+--------+-----------+--------------------------------------------------+-----------------------------+--------------------------------------------------------+----------------+

## Pasos para instalar la api:

	1) Clonar lrestful
	2) Instalar la base de datos adjunta en el proyecto
	3) Ejecutar composer update --no-scripts
	4) Renombrar el archivo .env.example a .env y agregar los datos de la BD
	5) Ejecutar php artisan key:generate
	6) Testear la api con postman o cualquier otra herramienta
-- --------------------------------------------------------
-- Host:                         localhost
-- Versión del servidor:         5.7.19 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para lrestful
CREATE DATABASE IF NOT EXISTS `lrestful` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `lrestful`;

-- Volcando estructura para tabla lrestful.aviones
CREATE TABLE IF NOT EXISTS `aviones` (
  `serie` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `modelo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitud` double(8,2) NOT NULL,
  `capacidad` int(11) NOT NULL,
  `velocidad` int(11) NOT NULL,
  `alcance` int(11) NOT NULL,
  `fabricante_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`serie`),
  KEY `aviones_fabricante_id_foreign` (`fabricante_id`),
  CONSTRAINT `aviones_fabricante_id_foreign` FOREIGN KEY (`fabricante_id`) REFERENCES `fabricantes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla lrestful.aviones: ~19 rows (aproximadamente)
/*!40000 ALTER TABLE `aviones` DISABLE KEYS */;
INSERT INTO `aviones` (`serie`, `modelo`, `longitud`, `capacidad`, `velocidad`, `alcance`, `fabricante_id`) VALUES
	(1, 'commodi', 129.88, 228, 6426, 823099651, 4),
	(2, 'in', 99.22, 362, 1099, 68, 2),
	(3, 'eos', 95.77, 808, 7861, 7422, 1),
	(4, 'harum', 133.70, 871, 1809, 2830860, 1),
	(5, 'quia', 87.02, 344, 8184, 6, 3),
	(6, 'earum', 57.54, 422, 1980, 9, 1),
	(7, 'possimus', 92.93, 329, 1537, 55762, 1),
	(8, 'veritatis', 98.18, 536, 3323, 3, 2),
	(9, 'necessitatibus', 138.45, 163, 5543, 5663, 3),
	(10, 'saepe', 115.42, 277, 5187, 50744509, 1),
	(11, 'consequatur', 51.69, 210, 6547, 585244228, 3),
	(12, 'quia', 143.19, 118, 9335, 5619, 4),
	(13, 'dolorum', 93.09, 753, 8317, 91115, 2),
	(14, 'est', 136.61, 85, 8787, 90484, 3),
	(15, 'sapiente', 117.64, 105, 8394, 243, 3),
	(16, 'dolorem', 32.12, 788, 3137, 5072254, 4),
	(17, 'ex', 64.96, 472, 4656, 5250, 2),
	(18, 'nihil', 23.16, 972, 5846, 500, 3),
	(19, 'sint', 138.76, 839, 4040, 0, 1);
/*!40000 ALTER TABLE `aviones` ENABLE KEYS */;

-- Volcando estructura para tabla lrestful.fabricantes
CREATE TABLE IF NOT EXISTS `fabricantes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla lrestful.fabricantes: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `fabricantes` DISABLE KEYS */;
INSERT INTO `fabricantes` (`id`, `nombre`, `direccion`, `telefono`) VALUES
	(1, 'eaque', 'impedit', 453091651),
	(2, 'nulla', 'accusamus', 726747534),
	(3, 'minima', 'vel', 524858978),
	(4, 'temporibus', 'nisi', 975560339);
/*!40000 ALTER TABLE `fabricantes` ENABLE KEYS */;

-- Volcando estructura para tabla lrestful.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla lrestful.migrations: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(13, '2014_10_12_000000_create_users_table', 1),
	(14, '2014_10_12_100000_create_password_resets_table', 1),
	(15, '2018_05_10_185424_fabricantes_migration', 1),
	(16, '2018_05_10_185501_aviones_migration', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Volcando estructura para tabla lrestful.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla lrestful.password_resets: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Volcando estructura para tabla lrestful.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla lrestful.users: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `email`, `password`, `created_at`, `updated_at`) VALUES
	(1, 'test@gamil.com', '$2y$10$tVMAWTIwJP73tjXwdb.nAu1RPjpYmHUjbtkqZJU3c8jFMPrVZa4UO', '2018-05-11 13:04:08', '2018-05-11 13:04:08');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

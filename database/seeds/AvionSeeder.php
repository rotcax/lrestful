<?php

use Illuminate\Database\Seeder;
use App\Fabricante;
use App\Avion;
use Faker\Factory as Faker;

class AvionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $cuantos = Fabricante::all()->count();

        for($i=0; $i<19; $i++){

        	Avion::create(
        		[
        			'modelo'=>$faker->word(),
        			'longitud'=>$faker->randomFloat(2, 10, 150),
        			'capacidad'=>$faker->randomNumber(3),
        			'velocidad'=>$faker->randomNumber(4),
        			'alcance'=>$faker->randomNumber(),
        			'fabricante_id'=>$faker->numberBetween(1, $cuantos)
        		]
        	);
        }
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(array('prefix'=>'api/v1'), function(){
	Route::resource('fabricantes', 'FabricanteController', ['except'=>['edit', 'create']]);
	Route::resource('fabricantes.aviones', 'FabricanteAvionController', ['except'=>['show', 'edit', 'create']]);
	Route::resource('aviones', 'AvionController', ['only'=>['index', 'show']]);
});